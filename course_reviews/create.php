<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate product object
include_once '../objects/course_reviews.php';
  
$database = new Database();
$db = $database->getConnection();
  
$courseReviews = new CourseReviews($db);


// get posted data
$data = json_decode(file_get_contents("php://input"));


// read if email exists
// query products
/*
$data->entityid = 1;
$data->comment = 'Hello';
$data->rating = 1;
$data->created_by = 1;*/



// make sure data is not empty
if(
    !empty($data->entityid) &&
    
    !empty($data->rating) &&
    !empty($data->created_by)
){
  
    // set product property values
    $courseReviews->entityid = $data->entityid;
    $courseReviews->entitytypeid = "Learner Comment";
    
    if(empty($data->comment)){
        $courseReviews->comment = NULL;
    }else{
        $courseReviews->comment = $data->comment;
    }
    
    $courseReviews->rating = $data->rating;
    $courseReviews->status = 1;
    $courseReviews->created_by = $data->created_by;
    
    $courseReviews->created_date = date('Y-m-d');
  
    // create the product
    if($courseReviews->create()){
    
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
       /* print_r($courseReviews);*/
        echo json_encode(array("message" => "Review Added Successfully"));
    }
  
    // if unable to create the product, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        
        echo json_encode(array("message" => "Unable to create, Please try again"));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Please Fill mandatory Fields."));
}
?>