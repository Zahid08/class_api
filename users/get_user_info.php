<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here
// include database and object files
include_once '../config/database.php';
include_once '../objects/users.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$users = new Users($db);

// set ID property of record to read
$users->unique_id = isset($_GET['unique_id']) ? $_GET['unique_id'] : die();


  
// read products will be here
// query products
$stmt = $users->readuser();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // products array
    $users_arr=array();
    $users_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        
        $users_data=array(
            "username" => $username,
            "email_id" => $email_id,
            "mobile"=> $mobile,
            "unique_id" => $unique_id
        );
        
         array_push($users_arr["records"], $users_data);
         
         
          // set response code - 200 OK
    http_response_code(200);
    echo json_encode(
        array("userdata" => $users_arr["records"])
        );
    }
  
}
  
// no products found will be here
else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no products found
    echo json_encode(
        array("message" => "No Users found.Please Enter Correct Credentials")
    );
}