<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate product object
include_once '../objects/users.php';
  
$database = new Database();
$db = $database->getConnection();
  
$users = new Users($db);




// get posted data
$data = json_decode(file_get_contents("php://input"));


/*$data->email_id = 'maidfdsfdl@ail.cm';
$data->password = 'Kavya@1234';
$data->username = 'Kavya';
$data->mobile = '6304389771';
$data->dob = '342343';
$data->city = 'city';*/

// read if email exists
// query products
$checkstmt = $users->checkIfExists($data->email_id);
$num = $checkstmt->rowCount();

if($num <= 0){
// make sure data is not empty
if(
    !empty($data->email_id) &&
    !empty($data->password) &&
    !empty($data->username) &&
    !empty($data->mobile) &&
    !empty($data->dob) &&
    !empty($data->city)
){
  
    // set product property values
    $users->unique_id = 'CEG'.rand(1000,9999).rand(10,99);
    $users->email_id = $data->email_id;
    $users->password = md5($data->password);
    $users->username = $data->username;
    $users->dob = $data->dob;
    $users->mobile = $data->mobile;
    $users->city = $data->city;
    $users->created_by = $data->email_id;
    $users->status = 1;
    $users->first_login = 2;
    $users->role = 3;
    
    
    if(!empty($data->parent_id)){
       $users->parent_id = $data->parent_id; 
    }else{
       $users->parent_id = NULL;  
    }
    
    
    
    $users->created_datetime = date('Y-m-d H:i:s');
  
    // create the product
    if($users->create()){
    
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
       /* print_r($users);*/
        echo json_encode(array("uid" => $users->unique_id));
    }
  
    // if unable to create the product, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        
        echo json_encode(array("message" => "Unable to create, Please try again"));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Please Fill mandatory Fields."));
}
}else{
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Email Already Exists."));
}
?>