<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here
// include database and object files
include_once '../config/database.php';
include_once '../objects/coursepromotion.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$coursepromotion = new Coursepromotion($db);



  
// read products will be here
// query products
$stmt = $coursepromotion->read();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // products array
    $coursepromotion_arr=array();
    $coursepromotion_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        $coursepromotion_data=array(
            "promotecourse_id" => $promotecourse_id,
            "image_url" => "https://competitiveexamguide.com/class/assets/courseImages_promotion/".$image_url
        );
         array_push($coursepromotion_arr["records"], $coursepromotion_data);
         
    }
    
    // set response code - 200 OK
    http_response_code(200);
    echo json_encode(
        array("promotecourses" => $coursepromotion_arr["records"])
        );
    
}
  
// no products found will be here
else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no products found
    echo json_encode(
        array("message" => "No Courses Found.")
    );
}