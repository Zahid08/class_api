<?php
class Users{
  
    // database connection and table name
    private $conn;
    private $table_name = "users";
  
    // object properties
    public $id;
    public $username;
    public $email_id;
    public $password;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    // read products
    function read(){
      $getEmailID = $_GET['email_id'];
        // select all query
        $query = "SELECT
                    u.id,u.unique_id,u.username,u.email_id,u.password
                FROM
                    " . $this->table_name . " u
                    WHERE
                    email_id = '$getEmailID'";
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
    
    // read products
    function readuser(){
      $getUniqueID = $_GET['unique_id'];
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " u
                    WHERE
                    unique_id = '$getUniqueID'";
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
    
    function checkIfExists($emailID){
        $checkIfExists = "SELECT
                    email_id
                FROM
                    " . $this->table_name . " u
                    WHERE
                    email_id='$emailID'";
        
        // prepare query statement
        $checkstmt = $this->conn->prepare($checkIfExists);
      
        // execute query
        $checkstmt->execute();
        return $checkstmt;
    }
    
    
    // create product
    function create(){
      
        
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    unique_id=:unique_id, email_id=:email_id, parent_id=:parent_id, username=:username, password=:password
                    , mobile=:mobile, dob=:dob, city=:city, role=:role, first_login=:first_login, status=:status, created_datetime=:created_datetime, created_by=:created_by";
      
        // prepare query
        $stmt = $this->conn->prepare($query);
      
        // sanitize
        $this->unique_id=htmlspecialchars(strip_tags($this->unique_id));
        $this->email_id=htmlspecialchars(strip_tags($this->email_id));
        $this->parent_id=htmlspecialchars(strip_tags($this->parent_id));
        $this->username=htmlspecialchars(strip_tags($this->username));
        $this->password=htmlspecialchars(strip_tags($this->password));
        $this->mobile=htmlspecialchars(strip_tags($this->mobile));
        $this->dob=htmlspecialchars(strip_tags($this->dob));
        $this->city=htmlspecialchars(strip_tags($this->city));
        $this->role=htmlspecialchars(strip_tags($this->role));
        $this->first_login=htmlspecialchars(strip_tags($this->first_login));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->created_datetime=htmlspecialchars(strip_tags($this->created_datetime));
        $this->created_by=htmlspecialchars(strip_tags($this->created_by));
      
        // bind values
        $stmt->bindParam(":unique_id", $this->unique_id);
        $stmt->bindParam(":email_id", $this->email_id);
        $stmt->bindParam(":parent_id", $this->parent_id);
        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":mobile", $this->mobile);
        $stmt->bindParam(":dob", $this->dob);
        $stmt->bindParam(":city", $this->city);
        $stmt->bindParam(":role", $this->role);
        $stmt->bindParam(":first_login", $this->first_login);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":created_datetime", $this->created_datetime);
        $stmt->bindParam(":created_by", $this->created_by);
      
        // execute query
        if($stmt->execute()){
            return true;
        }
      
        return false;
          
    }
    
}
?>