<?php
class Transactions{
  
    // database connection and table name
    private $conn;
    private $table_name = "transactions";
  
   
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    
    // create product
    function create(){
      
        
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    user_id=:user_id, course_id=:course_id, transaction_type=:transaction_type, status=:status, amount=:amount
                    , transaction_code=:transaction_code, created_date=:created_date, created_by=:created_by";
      
        // prepare query
        $stmt = $this->conn->prepare($query);
      
        // sanitize
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->course_id=htmlspecialchars(strip_tags($this->course_id));
        $this->transaction_type=htmlspecialchars(strip_tags($this->transaction_type));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->amount=htmlspecialchars(strip_tags($this->amount));
        $this->transaction_code=htmlspecialchars(strip_tags($this->transaction_code));
        $this->created_date=htmlspecialchars(strip_tags($this->created_date));
        $this->created_by=htmlspecialchars(strip_tags($this->created_by));
      
        // bind values
        $stmt->bindParam(":user_id", $this->user_id);
        $stmt->bindParam(":course_id", $this->course_id);
        $stmt->bindParam(":transaction_type", $this->transaction_type);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":amount", $this->amount);
        $stmt->bindParam(":transaction_code", $this->transaction_code);
        $stmt->bindParam(":created_date", $this->created_date);
        $stmt->bindParam(":created_by", $this->created_by);
      
        // execute query
        if($stmt->execute()){
            return true;
        }
      
        return false;
          
    }
    
}
?>