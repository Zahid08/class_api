<?php
class CoursePrice{
  
    // database connection and table name
    private $conn;
    private $table_name = "course_price";
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    // read products
    function read(){
      $getCourseID = $_GET['course_id'];
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " u
                    WHERE
                    course_id = $getCourseID
                    AND status = 1";
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
    
    
}
?>