<?php
class Course{
  
    // database connection and table name
    private $conn;
    private $table_name = "course";
  
   
    
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    // read products
    function read(){
        $user_id = $_GET['user_id'];
        
        $searchcriteria = $_GET['searchcriteria'];
        // select all query
        if(!empty($user_id)){
            $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " c
                    INNER JOIN transactions t ON t.course_id = c.course_id
                    
                   WHERE c.status = 1 AND (t.user_id = (SELECT id from users where unique_id='$user_id') OR t.user_id = ((SELECT unique_id from users where unique_id='$user_id')))";
         
        }
        else if(!empty($searchcriteria)){
            $query = "SELECT
                    *,(SELECT MIN(payable_price) FROM course_price cp where cp.course_id = c.course_id) as price,
                    (SELECT ROUND(AVG(rating)) from comments cm where cm.entityid = c.course_id AND
                    cm.entitytypeid='Learner Comment' and status = 1 GROUP BY entityid) as rating
                FROM
                    " . $this->table_name . " c
                   WHERE c.status = 1 AND c.title LIKE '%$searchcriteria%'";
        }else{
           $query = "SELECT
                    *,(SELECT MIN(payable_price) FROM course_price cp where cp.course_id = c.course_id) as price,
                    (SELECT ROUND(AVG(rating)) from comments cm where cm.entityid = c.course_id AND 
                    cm.entitytypeid='Learner Comment' and status = 1 GROUP BY entityid) as rating
                FROM
                    " . $this->table_name . " c
                   WHERE c.status = 1"; 
        }
        
        
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
    
    function read_content(){
        
        // select all query
        $getCourseID = $_GET["course_id"];
            $query = "SELECT sc.course_id
            		,sc.chapterid
            		,CASE sc.chapterid
                     WHEN sc.chapterid = 0 THEN 'No Chapter'
                    ELSE  (SELECT chapter_name FROM chapters c where c.id = sc.chapterid)
                    END
                    AS chapter_name
            		,sc.id AS sub_chapter_id
            		,sc.subchapter_name
            		,sc.type
            		,sc.sub_type
            		,sc.pre_class_msg
            		,sc.post_class_msg
            		,sc.learner_watched
            		,sc.show_recording
            		,sc.available_date
            		,sc.available_time
            		,sc.end_session
            		,(SELECT link from live_class_recordings lcr where lcr.sub_chapter_id = sc.id) as recording_link
            		,CASE 
            		 WHEN sc.sub_type = 'public_url' THEN sc.path
                	 WHEN sc.sub_type = 'upload' THEN CONCAT('https://competitiveexamguide.com/class/',sc.path) 
                	 WHEN sc.sub_type = 'bbb' THEN CONCAT('https://competitiveexamguide.com/class/CourseLearners?courseid=$getCourseID&itemid=',sc.id)
                	 ELSE sc.path 
                	 END AS path
            		,sc.created_date
            	FROM `sub_chapters` sc
            	WHERE sc.course_id = $getCourseID
            		AND sc.STATUS = 1
            ORDER BY sc.created_date";
                 
        
      
        
        
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
    
  
    
}
?>