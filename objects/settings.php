<?php
class Settings{
  
    // database connection and table name
    private $conn;
    private $table_name = "setting";
  
   
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    // read products
    function read(){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " s
                    WHERE
                    name = 'Version'";
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
   
    
}
?>