<?php
class Quiz{
  
    // database connection and table name
    private $conn;
    private $table_quiz                 = "quiz_test";
    private $table_quiz_test_question   = "quiz_test_questions";
    private $table_quiz_test_options    = "quiz_test_question_options";


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function readSingleQuiz($courseId=null,$quizId){
        // select single quiz query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_quiz . "
                WHERE id=$quizId";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readSingleQuestions($quizId,$questionId,$subject=null){
        // select single question query
        if ($questionId){
            $query = "SELECT id,quiz_test_id,type,subject,topic,question_text,question_text_lang,explaination,explaination_lang,mark,penalty,answar_status,quiz_type FROM quiz_test_questions WHERE id = ".$questionId."";
        }elseif ($subject){
            $query = "SELECT id,quiz_test_id,type,subject,topic,question_text,question_text_lang,explaination,explaination_lang,mark,penalty,answar_status,quiz_type FROM quiz_test_questions WHERE quiz_test_id = ".$quizId." AND subject LIKE '".$subject."'";
        }else{
            $query = "SELECT id,quiz_test_id,type,subject,topic,question_text,question_text_lang,explaination,explaination_lang,mark,penalty,answar_status,quiz_type FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId."  ORDER BY id";
        }
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readQuestionsOptions($questionId){

        // select single question Options query
        $query = "SELECT id,option_text,option_text_lang FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$questionId." ";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readSelectedOptions($questionId,$userId=''){

        // select single question Options query
        $query = "SELECT * FROM quiz_test_temporary_attempt WHERE user_id =$userId AND question_id= ".$questionId." ";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function readPreviousQuestion($questionId,$quizId){

        // select single question Options query
        $query = "select * from quiz_test_questions where id = (select max(id) from quiz_test_questions where id < $questionId) AND status = 1 AND quiz_test_id = ".$quizId." ";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        $singleRowQuestion = $stmt->fetch(PDO::FETCH_ASSOC);

        $returnPreviousId='';
        if ($singleRowQuestion){
            $returnPreviousId=$singleRowQuestion['id'];
        }

        return $returnPreviousId;
    }

    function readNextQuestion($questionId,$quizId){

        // select single question Options query
        $query = "SELECT * FROM quiz_test_questions WHERE id > ".$questionId." AND status = 1 AND quiz_test_id = ".$quizId."";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        $singleRowQuestion = $stmt->fetch(PDO::FETCH_ASSOC);

        $returnPreviousId='';
        if ($singleRowQuestion){
            $returnPreviousId=$singleRowQuestion['id'];
        }

        return $returnPreviousId;
    }


    function readSubjectByQuizId($quizId){
        // select Subject query
        $query = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." GROUP BY subject ";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function updateQuestionAnswer($questionId,$questionOptionId,$valueOfSubjective='',$quizTyeId,$quizId,$userId=''){

        $queryTemporaryAttempt = "SELECT * FROM quiz_test_temporary_attempt WHERE user_id =$userId AND question_id =".$questionId."";
        // prepare query statement
        $stmtTemporaryAttempt = $this->conn->prepare($queryTemporaryAttempt);
        // execute query
        $stmtTemporaryAttempt->execute();
        $existColumnStatement = $stmtTemporaryAttempt->rowCount();
        $singleRowTemporaryAttempt = $stmtTemporaryAttempt->fetch(PDO::FETCH_ASSOC);

        if ($quizTyeId==4 || $quizTyeId==3){
            if ($existColumnStatement){
                $query = "UPDATE quiz_test_temporary_attempt SET answer_option_text ='".$valueOfSubjective."' WHERE id= '".$singleRowTemporaryAttempt['id']."' and  user_id=$userId";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
            }else{
                // query to insert record
                $queryInsertAttemptQuery= "INSERT INTO quiz_test_temporary_attempt SET
                    user_id=:user_id, quiz_test_id=:quiz_test_id, question_id=:question_id, answer_option_text=:answer_option_text
                    , answer_option_file=:answer_option_file";

                $queryInsertAttempt = $this->conn->prepare($queryInsertAttemptQuery);

                $filepath='';
                // bind values
                $queryInsertAttempt->bindParam(":user_id", $userId);
                $queryInsertAttempt->bindParam(":quiz_test_id", $quizId);
                $queryInsertAttempt->bindParam(":question_id", $questionId);
                $queryInsertAttempt->bindParam(":answer_option_text",$valueOfSubjective);
                $queryInsertAttempt->bindParam(":answer_option_file",$filepath);
                $queryInsertAttempt->execute();
            }

        }else{
            if ($existColumnStatement){
                $id=$singleRowTemporaryAttempt['id'];
                $queryUpdate1 = "UPDATE quiz_test_temporary_attempt SET selected_answer ='".$questionOptionId."' WHERE id=$id and  user_id=$userId";
                $stmtupdate1 = $this->conn->prepare($queryUpdate1);
                $stmtupdate1->execute();
            }else{
                // query to insert record
                $queryInsertAttemptQuery= "INSERT INTO quiz_test_temporary_attempt SET
                    user_id=:user_id, quiz_test_id=:quiz_test_id, question_id=:question_id, selected_answer=:selected_answer";

                $queryInsertAttempt = $this->conn->prepare($queryInsertAttemptQuery);

                $filepath='';
                // bind values
                $queryInsertAttempt->bindParam(":user_id", $userId);
                $queryInsertAttempt->bindParam(":quiz_test_id", $quizId);
                $queryInsertAttempt->bindParam(":question_id", $questionId);
                $queryInsertAttempt->bindParam(":selected_answer",$questionOptionId);
                $queryInsertAttempt->execute();
            }
        }

        //Update Question Answer
        $queryQuestionUpdate = "UPDATE quiz_test_questions SET answar_status =1 WHERE id = '". $questionId."' ";
        $stmtQuestionUpdate = $this->conn->prepare($queryQuestionUpdate);
        $stmtQuestionUpdate->execute();


        // Calcualted TotalAnswer
        $queryQuestion = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." ";
        $stmtQuestions = $this->conn->prepare($queryQuestion);
        $stmtQuestions->execute();

        $answarStatus=1;
        $queryQuestionAnswer = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." AND answar_status = ".$answarStatus." ";
        $stmtQuestionsAnswer = $this->conn->prepare($queryQuestionAnswer);
        $stmtQuestionsAnswer->execute();

        $totalQuestions                =$stmtQuestions->rowCount();
        $totalAnswar                   = $stmtQuestionsAnswer->rowCount();
        $notAnswar                      =$totalQuestions-$totalAnswar;

        $selectedOptions=unserialize($questionOptionId);
        $dataArray=array(
            'answer'=>$totalAnswar,
            'not_answer'=>$notAnswar,
            'selected_options'=>$selectedOptions,
            'set_subjective'=>$valueOfSubjective,
        );

        return $dataArray;
    }

    function removedQuestionAnswer($quizId,$questionId,$userId){
        //Remvoed Question Option Selected
        $query = "UPDATE quiz_test_temporary_attempt SET selected_answer ='',answer_option_text='',answer_option_file='' WHERE quiz_test_id=$quizId and question_id=$questionId and user_id=$userId";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        //Remvoed Question Option Selected
        $query1 = "UPDATE quiz_test_questions SET answar_status ='' WHERE id ='". $questionId."'";
        $stmt1 = $this->conn->prepare($query1);
        $stmt1->execute();

        // Calcualted TotalAnswer
        $queryQuestion = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." ";
        $stmtQuestions = $this->conn->prepare($queryQuestion);
        $stmtQuestions->execute();

        $answarStatus=1;
        $queryQuestionAnswer = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." AND answar_status = ".$answarStatus." ";
        $stmtQuestionsAnswer = $this->conn->prepare($queryQuestionAnswer);
        $stmtQuestionsAnswer->execute();

        $totalQuestions                =$stmtQuestions->rowCount();
        $totalAnswar                   = $stmtQuestionsAnswer->rowCount();
        $notAnswar                      =$totalQuestions-$totalAnswar;

        $dataArray=array(
            'answer'=>$totalAnswar,
            'not_answer'=>$notAnswar,
        );

        return $dataArray;
    }

    function finalQuizAttempt($quizId,$totalAttemptTime,$userId){

        // Get QUiz Test
        $query1 = "SELECT * FROM quiz_test WHERE status = 1 AND id=$quizId";
        $stmt1 = $this->conn->prepare($query1);
        $stmt1->execute();
        $quiztest = $stmt1->fetch(PDO::FETCH_ASSOC);

        // Calcualted TotalAnswer
        $queryQuestion = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." ";
        $stmtQuestions = $this->conn->prepare($queryQuestion);
        $stmtQuestions->execute();

        $answarStatus=1;
        $queryQuestionAnswer = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." AND answar_status = ".$answarStatus." ";
        $stmtQuestionsAnswer = $this->conn->prepare($queryQuestionAnswer);
        $stmtQuestionsAnswer->execute();

        $totalQuestions                =$stmtQuestions->rowCount();
        $totalAnswar                   = $stmtQuestionsAnswer->rowCount();
        $notAnswar                      =$totalQuestions-$totalAnswar;


        // Check Subjective Status
        $query2 = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId."   AND type =4";
        $stmt2 = $this->conn->prepare($query2);
        $stmt2->execute();

        $markCompleted=1;
        if ($stmt2->rowCount()>0 || $quiztest['quiz_type']==1){
            $markCompleted=0;
        }

        // query to insert record
        $queryInsertAttemptQuery= "INSERT INTO quiz_test_attempt SET
                    quiztest_id=:quiztest_id, user_id=:user_id, take_attempt_time=:take_attempt_time, platform=:platform
                    , total_question=:total_question, answar_question=:answar_question, mark_complete_status=:mark_complete_status";

        $queryInsertAttempt = $this->conn->prepare($queryInsertAttemptQuery);

        // bind values
        $platForm='phone';
        $queryInsertAttempt->bindParam(":quiztest_id", $quizId);
        $queryInsertAttempt->bindParam(":user_id", $userId);
        $queryInsertAttempt->bindParam(":take_attempt_time", $totalAttemptTime);
        $queryInsertAttempt->bindParam(":platform", $platForm);
        $queryInsertAttempt->bindParam(":total_question", $totalQuestions);
        $queryInsertAttempt->bindParam(":answar_question", $totalAnswar);
        $queryInsertAttempt->bindParam(":mark_complete_status", $markCompleted);

        // execute query
        if($queryInsertAttempt->execute()){
            $LAST_ID = $this->conn->lastInsertId();
            if ($LAST_ID){
                while ($questionitem = $stmtQuestions->fetch(PDO::FETCH_ASSOC)) {

                    $selectedAnswar=1;
                    $studentGivenAnswerText=$correctAnserText=$studentAttachFile='';

                    $dataOptionsIdArray=array();
                    if ($questionitem['type']==3 || $questionitem['type']==4){

                        $getOptionsIdQuery = "SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = ".$questionitem['id'] ." ";
                        $getOptionsIdStatement = $this->conn->prepare($getOptionsIdQuery);
                        $getOptionsIdStatement->execute();

                        $coorectOptionsIdQuery = "SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem['id'] . "  ";
                        $coorectOptionsIdStatement = $this->conn->prepare($coorectOptionsIdQuery);
                        $coorectOptionsIdStatement->execute();

                        $singleAnswerOptionsQuery = "SELECT * FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem['id'] . "";
                        $answerTextStatement1 = $this->conn->prepare($singleAnswerOptionsQuery);
                        $answerTextStatement1->execute();
                        $singleRowanswerText1= $answerTextStatement1->fetch(PDO::FETCH_ASSOC);


                        $answerTextQuery = "SELECT * FROM quiz_test_temporary_attempt WHERE question_id= " . $questionitem['id'] ." and user_id=$userId";
                        $answerTextStatement = $this->conn->prepare($answerTextQuery);
                        $answerTextStatement->execute();
                        $singleRowanswerText= $answerTextStatement->fetch(PDO::FETCH_ASSOC);

                        $studentGivenAnswerText =$singleRowanswerText['answer_option_text'];
                        $studentAttachFile      =$singleRowanswerText['answer_option_file'];
                        $correctAnserText       =$singleRowanswerText1['option_text'];

                        if (!empty($getOptionsIdStatement)) {
                            while ($rowOptionsItem = $getOptionsIdStatement->fetch(PDO::FETCH_ASSOC)){
                                array_push($dataOptionsIdArray, $rowOptionsItem['id']);
                            }
                        }

                    }else {
                        $getOptionsIdQuery = "SELECT * FROM quiz_test_temporary_attempt WHERE question_id= ".$questionitem['id'] ."  and user_id=$userId";
                        $getOptionsIdStatement = $this->conn->prepare($getOptionsIdQuery);
                        $getOptionsIdStatement->execute();
                        $choseSelectedItems = $getOptionsIdStatement->fetch(PDO::FETCH_ASSOC);
                        if ($choseSelectedItems){
                            $dataOptionsIdArray=unserialize($choseSelectedItems['selected_answer']);
                        }

                        $coorectOptionsIdQuery = "SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem['id'] . " AND is_answer = " . $selectedAnswar ."";
                        $coorectOptionsIdStatement = $this->conn->prepare($coorectOptionsIdQuery);
                        $coorectOptionsIdStatement->execute();
                    }

                    $dataOptionsCorrectIdArray=array();
                    if (!empty($coorectOptionsIdStatement)) {
                        while ($rowOptionsItemCorrect = $coorectOptionsIdStatement->fetch(PDO::FETCH_ASSOC)){
                            array_push($dataOptionsCorrectIdArray, $rowOptionsItemCorrect['id']);
                        }
                    }

                   // if (count($dataOptionsIdArray)>0){

                        //Save Attempt Result
                        $attemptResultQuery= "INSERT INTO quiz_test_attempt_result SET 
                    user_id=:user_id
                    attempt_id=:attempt_id, 
                    quiz_test_id=:quiz_test_id,
                    question_type=:question_type,
                     question_id=:question_id,
                     chose_option_id=:chose_option_id,
                     correct_chose_option_id=:correct_chose_option_id,
                    chose_option_text=:chose_option_text,
                    correct_option_text=:correct_option_text,
                    chose_option_file=:chose_option_file";

                        $insertQuizAttemptResultData = $this->conn->prepare($attemptResultQuery);

                        // bind values
                        $dataOptionsIdArray=serialize($dataOptionsIdArray);
                        $dataOptionsCorrectIdArray=serialize($dataOptionsCorrectIdArray);

                        $questionType=$questionitem['type'];
                        $questionitemID=$questionitem['id'];


                        $insertQuizAttemptResultData->bindParam(":user_id", $userId);
                        $insertQuizAttemptResultData->bindParam(":attempt_id", $LAST_ID);
                        $insertQuizAttemptResultData->bindParam(":quiz_test_id", $quizId);
                        $insertQuizAttemptResultData->bindParam(":question_type", $questionType);
                        $insertQuizAttemptResultData->bindParam(":question_id", $questionitemID);
                        $insertQuizAttemptResultData->bindParam(":chose_option_id", $dataOptionsIdArray);
                        $insertQuizAttemptResultData->bindParam(":correct_chose_option_id",$dataOptionsCorrectIdArray);
                        $insertQuizAttemptResultData->bindParam(":chose_option_text", $studentGivenAnswerText);
                        $insertQuizAttemptResultData->bindParam(":correct_option_text", $correctAnserText);
                        $insertQuizAttemptResultData->bindParam(":chose_option_file", $studentAttachFile);

                        if($insertQuizAttemptResultData->execute()) {

                            $last_attempt_result_id = $this->conn->lastInsertId();

                            if ($last_attempt_result_id) {
                                $queryupdateQuestionSelectedAnswarOptions = "UPDATE quiz_test_temporary_attempt SET selected_answer ='',answer_option_text ='',answer_option_file='' WHERE question_id ='". $questionitem['id']."' and user_id=$userId";
                                $stmtupdateQuestionSelectedAnswarOptions = $this->conn->prepare($queryupdateQuestionSelectedAnswarOptions);
                                $stmtupdateQuestionSelectedAnswarOptions->execute();

                                $queryselectedAnswarStatus= "UPDATE quiz_test_questions SET answar_status ='' WHERE id ='". $questionitem['id']."'";
                                $stmtSelectedAnswarStatus = $this->conn->prepare($queryselectedAnswarStatus);
                                $stmtSelectedAnswarStatus->execute();
                            }
                        }
                }


                $updatedValue=$this->markCalcualtions($LAST_ID,$quizId);

                $getMark        =$updatedValue['get_mark'];
                $correct        =$updatedValue['correct'];
                $incorect       =$updatedValue['incorect'];
                $mark_total     =$updatedValue['mark_total'];
                $skipped        =$updatedValue['skipped'];

                $queryLast= "UPDATE quiz_test_attempt SET get_mark =$getMark,correct =$correct,incorect =$incorect,mark_total =$mark_total,skipped =$skipped WHERE id = '". $LAST_ID."'";
                $queryLastStm = $this->conn->prepare($queryLast);
                $queryLastStm->execute();

                $returnData='';
                if ($markCompleted==0){
                    $returnData='unpublish';
                    return $returnData;
                }else {
                    $returnData='publish';
                    return $returnData;
                }
            }
        }
    }

      function markCalcualtions($attemptId,$quizTestId){

        $newQuestionsQuery = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." ORDER BY id DESC ";
        $newQuestionsStm = $this->conn->prepare($newQuestionsQuery);
        $newQuestionsStm->execute();

        $totalQuestions         =$newQuestionsStm->rowCount();
        $countCorrectAnswar     =0;
        $totalAttemptAnswar     =0;

        $countTotalCorrectAnswarMark=0;
        $countPenalityMark=0;
        $totalQuestionsMark=0;

        while ($getnewQuestions = $newQuestionsStm->fetch(PDO::FETCH_ASSOC)) {
            $totalQuestionsMark=$totalQuestionsMark+$getnewQuestions['mark'];

            $attemptResultQuery = "SELECT * FROM quiz_test_attempt_result WHERE question_id = ".$getnewQuestions['id']." AND attempt_id = ".$attemptId." ";
            $attemptResultStm = $this->conn->prepare($attemptResultQuery);
            $attemptResultStm->execute();

            $attemptResult = $attemptResultStm->fetch(PDO::FETCH_ASSOC);

            if ($attemptResult){

                $found='';

                if ($getnewQuestions['type']==3 || $getnewQuestions['type']==4){
                    $choseOptionsText = $attemptResult['chose_option_text'];
                    $correctOptionsText =$attemptResult['correct_option_text'];

                    if ($choseOptionsText==$correctOptionsText){
                        $found = true;
                    }

                }else {
                    if ($attemptResult['chose_option_id']==$attemptResult['correct_chose_option_id']){
                        $found = true;
                    }else{
                        $found = false;
                    }
                }

                if ($found==true){
                    $countCorrectAnswar++;
                    $countTotalCorrectAnswarMark=$countTotalCorrectAnswarMark+$getnewQuestions['mark'];
                }else{
                    $countPenalityMark=$countPenalityMark+$countTotalCorrectAnswarMark+$getnewQuestions['penalty'];
                }

                $totalAttemptAnswar++;
            }
        }

        $inCorrectAnswar=$totalAttemptAnswar-$countCorrectAnswar;
        $skippedQuestions=$totalQuestions-$totalAttemptAnswar;

        $markeAnswarTotal=$countTotalCorrectAnswarMark-$countPenalityMark;

        $dataList=array(
            'mark_total'            =>$totalQuestionsMark,
            'incorect'              =>$inCorrectAnswar,
            'skipped'               =>$skippedQuestions,
            'correct'               =>$countCorrectAnswar,
            'get_mark'              =>$markeAnswarTotal,
        );

        return $dataList;
    }

    //Get All Attempt Inforamtions
    function getAttemptInformations($userId,$quizId,$attemptId){

        //Question List
        $newQuestionsQuery = "SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizId." ORDER BY id DESC ";
        $newQuestionsStm = $this->conn->prepare($newQuestionsQuery);
        $newQuestionsStm->execute();

        $getAllAttemptsQuery = "SELECT * FROM quiz_test_attempt WHERE quiztest_id = ".$quizId." AND user_id = ".$userId." AND mark_complete_status =1 ORDER BY id DESC ";
        $getAllAttemptsStmList = $this->conn->prepare($getAllAttemptsQuery);
        $getAllAttemptsStmList->execute();

        $dataList=[];

        $count=1;
        while ($getAllAttempts = $getAllAttemptsStmList->fetch(PDO::FETCH_ASSOC)) {

                $totalQuestions         =$newQuestionsStm->rowCount();
                $countCorrectAnswar     =0;
                $totalAttemptAnswar     =0;

                $attemmptId             =$getAllAttempts['id'];
                $createDate             =$getAllAttempts['created_at'];

                $countTotalCorrectAnswarMark=0;
                $countPenalityMark          =0;
                $totalQuestionsMark         =0;

                //Match With Corrent And Incoreect Answer
                while ($getnewQuestions = $newQuestionsStm->fetch(PDO::FETCH_ASSOC)) {
                    $totalQuestionsMark=$totalQuestionsMark+$getnewQuestions['mark'];

                    $attemptResultQuery = "SELECT * FROM quiz_test_attempt_result WHERE question_id = ".$getnewQuestions['id']." AND attempt_id = ".$attemmptId." ";
                    $attemptResultStm   = $this->conn->prepare($attemptResultQuery);
                    $attemptResultStm->execute();
                    $attemptResult = $attemptResultStm->fetch(PDO::FETCH_ASSOC);

                    if ($attemptResult){

                        $found='';

                        if ($getnewQuestions['type']==3 || $getnewQuestions['type']==4){
                            $choseOptionsText = $attemptResult['chose_option_text'];
                            $correctOptionsText =$attemptResult['correct_option_text'];

                            if ($choseOptionsText==$correctOptionsText){
                                $found = true;
                            }

                        }else {
                            if ($attemptResult['chose_option_id']==$attemptResult['correct_chose_option_id']){
                                $found = true;
                            }else{
                                $found = false;
                            }
                        }

                        if ($found==true){
                            $countCorrectAnswar++;
                            $countTotalCorrectAnswarMark=$countTotalCorrectAnswarMark+$getnewQuestions['mark'];
                        }else{
                            $countPenalityMark=$countPenalityMark+$countTotalCorrectAnswarMark+$getnewQuestions['penalty'];
                        }

                        $totalAttemptAnswar++;
                    }
                }

                $inCorrectAnswar=$totalAttemptAnswar-$countCorrectAnswar;
                $skippedQuestions=$totalQuestions-$totalAttemptAnswar;
                $markeAnswarTotal=$countTotalCorrectAnswarMark-$countPenalityMark;


                $getAttemptInformationsQuery = "SELECT * FROM quiz_test_attempt WHERE id = ".$attemmptId." ";
                $getAllAttemptsStm   = $this->conn->prepare($getAttemptInformationsQuery);
                $getAllAttemptsStm->execute();
                $getSingleAttempt = $getAllAttemptsStm->fetch(PDO::FETCH_ASSOC);
                $getTime         =$getSingleAttempt['take_attempt_time'];


                $rankQuery = "SELECT id,get_mark, FIND_IN_SET( get_mark, (    
                              SELECT GROUP_CONCAT( get_mark
                              ORDER BY get_mark DESC ) 
                              FROM quiz_test_attempt WHERE quiztest_id= ".$quizId.")
                            ) AS rank
                            FROM quiz_test_attempt
                            WHERE quiztest_id= ".$quizId." AND id = ".$attemmptId."";

                $rankQueryStm   = $this->conn->prepare($rankQuery);
                $rankQueryStm->execute();
                $rankData = $rankQueryStm->fetch(PDO::FETCH_ASSOC);
                $rank=$rankData['rank'];

                $totalAttemptQuery = "SELECT * FROM quiz_test_attempt WHERE id = ".$attemmptId." ";
                $totalAttemptQueryStm   = $this->conn->prepare($totalAttemptQuery);
                $totalAttemptQueryStm->execute();
                $totalAttempt=$totalAttemptQueryStm->rowCount();

            $dataList[$attemmptId]=[
                'attemmptId'         =>$attemmptId,
                'create_date'         =>$createDate,
                'total_attempt_user' =>$totalAttempt,
                'rank'              =>$rank,
                'take_time'          =>$getTime,
                'gain_mark'          =>$markeAnswarTotal,
                'total_mark'         =>$totalQuestionsMark,
                'total_question'      =>$totalQuestions,
                'correct_question'    =>$countCorrectAnswar,
                'incorrect_question'  =>$inCorrectAnswar,
                'skip_question'       =>$skippedQuestions,
            ];
        }

        if ($attemptId){
            return $dataList[$attemptId];
        }else{
            return $dataList;
        }
    }

    function leftAttempt($quizId,$userId='',$setAttempt){

        // select single question Options query
        $query = "SELECT * FROM quiz_test_attempt WHERE quiztest_id =$quizId and user_id =$userId  ORDER BY id DESC";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        $totalAttempt=$stmt->rowCount();
        $resultAttempt=$setAttempt-$totalAttempt;

        return $resultAttempt;
    }
}
?>