<?php
class CourseReviews{
  
    // database connection and table name
    private $conn;
    private $table_name = "comments";
  
    
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    // read products
    function read(){
      $getCourseID = $_GET['course_id'];
        // select all query
        $query = "SELECT
                    *,u.username
                FROM
                    " . $this->table_name . " 
                    c inner join users u on u.id = c.created_by
                    WHERE
                    entityid = '$getCourseID'
                    AND c.status = 1
                    AND entitytypeid = 'Learner Comment'";
                    
      
        // prepare query statement
        $stmt = $this->conn->prepare($query);
      
        // execute query
        $stmt->execute();
      
        return $stmt;
    }
    
    // create product
    function create(){
      
        
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    entityid=:entityid
                    ,entitytypeid=:entitytypeid
                    ,comment=:comment
                    ,rating=:rating
                    ,status=:status
                    ,created_date=:created_date
                    ,created_by=:created_by";
      
        // prepare query
        $stmt = $this->conn->prepare($query);
      
        // sanitize
        $this->entityid=htmlspecialchars(strip_tags($this->entityid));
        $this->entitytypeid=htmlspecialchars(strip_tags($this->entitytypeid));
        $this->comment=htmlspecialchars(strip_tags($this->comment));
        $this->rating=htmlspecialchars(strip_tags($this->rating));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->created_date=htmlspecialchars(strip_tags($this->created_date));
        $this->created_by=htmlspecialchars(strip_tags($this->created_by));
      
        // bind values
        $stmt->bindParam(":entityid", $this->entityid);
        $stmt->bindParam(":entitytypeid", $this->entitytypeid);
        $stmt->bindParam(":comment", $this->comment);
        $stmt->bindParam(":rating", $this->rating);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":created_date", $this->created_date);
        $stmt->bindParam(":created_by", $this->created_by);
      
        // execute query
        if($stmt->execute()){
            return true;
        }
      
        return false;
          
    }
    
    
    
}
?>