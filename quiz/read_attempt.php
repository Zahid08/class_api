<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate Quiz object
include_once '../objects/quiz.php';

$database = new Database();
$db = $database->getConnection();

$quizObject = new Quiz($db);

$userId         =isset($_REQUEST['userId'])?$_REQUEST['userId']:'';
$quizId         =isset($_REQUEST['quizId'])?$_REQUEST['quizId']:'';
$attemptId      =isset($_REQUEST['attemptId'])?$_REQUEST['attemptId']:'';


if ($userId && $quizId){
    $repsonseArray['attempt'] = $quizObject->getAttemptInformations($userId,$quizId,$attemptId);
    http_response_code(200);
    echo json_encode(
        array("status"=>200,"records" =>$repsonseArray)
    );
}else{

    // set response code - 400 bad request
    http_response_code(400);

    // tell the user
    echo json_encode(array("status"=>400,"message" => "Please Fill mandatory Fields (User Id , Course Id ,Quiz Id)."));
}