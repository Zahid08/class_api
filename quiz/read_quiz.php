<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate Quiz object
include_once '../objects/quiz.php';

$database = new Database();
$db = $database->getConnection();

$quizObject = new Quiz($db);

$userId     =isset($_REQUEST['userId'])?$_REQUEST['userId']:'';
$courseId   =isset($_REQUEST['courseId'])?$_REQUEST['courseId']:'';
$quizId     =isset($_REQUEST['quizId'])?$_REQUEST['quizId']:'';

/*$userId     =123;
$courseId   =5;
$quizId     =58;*/


if ($userId && $courseId && $quizId){

    $stmt = $quizObject->readSingleQuiz($courseId,$quizId);
    $num = $stmt->rowCount();
    $singleRow = $stmt->fetch(PDO::FETCH_ASSOC);
   
    $singleRow['available_from']=strtotime($singleRow['available_from']);
    $singleRow['available_till']=strtotime($singleRow['available_till']);
    
    $quiz_array["records"]=$singleRow;
    // set response code - 200 OK
    
    $readTotalQuestion = $quizObject->readSingleQuestions($singleRow['id'],'','','');

    $restultStatement =$readTotalQuestion->rowCount();
    
   
    // left Atttempt
    $retakeAttempt = $quizObject->leftAttempt($quizId,$userId,$singleRow['re_take_attempts']);
    
     $getStartTime = $quizObject->StartTime($quizId,$userId);
    $num = $getStartTime->rowCount();
    $StartTime = $getStartTime->fetch(PDO::FETCH_ASSOC);
   
    $start_time = $StartTime['start_time'];
    
    http_response_code(200);
    echo json_encode(
        array("status"=>200,"single_quiz" => $quiz_array["records"],"total_questions"=>$restultStatement,"left_attempt"=>$retakeAttempt,"start_time"=>$start_time,"current_time"=>time())
    );

}else{

    // set response code - 400 bad request
    http_response_code(400);

    // tell the user
    echo json_encode(array("status"=>400,"message" => "Please Fill mandatory Fields (User Id , Course Id ,Quiz Id)."));
}