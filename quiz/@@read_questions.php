<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate Quiz object
include_once '../objects/quiz.php';

$database = new Database();
$db = $database->getConnection();

$quizObject = new Quiz($db);

$userId     =isset($_REQUEST['userId'])?$_REQUEST['userId']:'';
$quizId     =isset($_REQUEST['quizId'])?$_REQUEST['quizId']:'';
$questionId =isset($_REQUEST['questionId'])?$_REQUEST['questionId']:'';
$status       =isset($_REQUEST['status'])?$_REQUEST['status']:'';
$subject    =isset($_REQUEST['subject'])?$_REQUEST['subject']:'';

if ($userId && $quizId){

    if ($status!='all' || $status=='') {  //Single Data
        //Get Questions
        $stmt = $quizObject->readSingleQuestions($quizId, $questionId,$subject);
        $num = $stmt->rowCount();
        $singleRowQuestion = $stmt->fetch(PDO::FETCH_ASSOC);
        $questionId = $singleRowQuestion['id'];


        //Get Question Options
        $stmtOptions = $quizObject->readQuestionsOptions($questionId);
        $repsonseArray['question'] = $singleRowQuestion;
        $repsonseArray["question_options"] = array();

        while ($row = $stmtOptions->fetch(PDO::FETCH_ASSOC)) {
            array_push($repsonseArray["question_options"], $row);
        }

        $previousQuestionId = $quizObject->readPreviousQuestion($questionId, $quizId);
        $nextQuestionId = $quizObject->readNextQuestion($questionId, $quizId);

        //Previous Next Questions Options
        $repsonseArray['currentQuestionId'] = $questionId;
        $repsonseArray['previousQuestionId'] = $previousQuestionId;
        $repsonseArray['nextQuestionId'] = $nextQuestionId;

        http_response_code(200);
        echo json_encode(
            array("status"=>200,"records" =>$repsonseArray)
        );
    }else{

        //Read All Question paper
        $stmt = $quizObject->readSingleQuestions($quizId,$questionId);
        $num = $stmt->rowCount();

        $repsonseArray["question"]    =array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            array_push($repsonseArray["question"], $row);
        }

        http_response_code(200);
        echo json_encode(
            array("status"=>200,"records" =>$repsonseArray)
        );
    }

}else{

    // set response code - 400 bad request
    http_response_code(400);

    // tell the user
    echo json_encode(array("status"=>400,"message" => "Please Fill mandatory Fields (User Id , Course Id ,Quiz Id)."));
}