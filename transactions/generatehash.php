<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get posted data
$data = json_decode(file_get_contents("php://input"));


/*$data->udf1 = "3"; // user_id
$data->amount = "3000"; 
$data->firstname = "kavya";
$data->email = "iambommanakavya@gmail.com";
$data->phone = "8097886765";
$data->productinfo = "5";*/ //course_id

if(
    !empty($data->udf1) &&
    !empty($data->amount) &&
    !empty($data->firstname) &&
    !empty($data->email) &&
    !empty($data->phone) &&
    !empty($data->productinfo)
){
    
    // Generate random transaction id
$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
/*$_POST['key'] = "hHO8Y2Ey";*/ //live
$_POST['key'] = "gtKFFx"; //test
$_POST['hash'] = "";
$_POST['txnid'] = $txnid;
$_POST['hash_abc'] = "";
$_POST['udf1'] = $data->udf1;
$_POST['amount'] = $data->amount;
$_POST['firstname'] = $data->firstname;
$_POST['email'] = $data->email;
$_POST['phone'] = $data->phone;
$_POST['productinfo'] = $data->productinfo;
$_POST['service_provider'] = "payu_paisa";
$_POST['surl'] = " ";
$_POST['furl'] = " ";
 
 
// Merchant key here as provided by Payu
/*$MERCHANT_KEY = "hHO8Y2Ey";*/ //production
$MERCHANT_KEY = "gtKFFx"; //test

// Merchant Salt as provided by Payu
/*$SALT = "sM2cmn3okT";*/ //live
$SALT = "eCwWELxi";


// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
    
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
 
}

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
    
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
          || empty($posted['udf1'])
          
      || empty($posted['service_provider'])
  ) {
      
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
  $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';  
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;
    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
    
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';

}


// set response code - 201 created
        http_response_code(201);
  
        // tell the user
       /* print_r($users);*/
        echo json_encode(array("hash" => $hash,"txnid"=>$txnid));

    
    
}else{
    // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        
        echo json_encode(array("message" => "Unable to create, Please try again"));
}

?>