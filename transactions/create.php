<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate product object
include_once '../objects/transactions.php';
  
$database = new Database();
$db = $database->getConnection();
  
$transactions = new Transactions($db);


// get posted data
$data = json_decode(file_get_contents("php://input"));


//Testing
/*$data->user_id = "CEG702118";
$data->course_id = 5;
$data->transaction_type = "razorpay";
$data->status = "success";
$data->amount = 3000;
$data->transaction_code = "Competithgk87t7q23";*/

// make sure data is not empty
if(
    !empty($data->user_id) &&
    !empty($data->course_id) &&
    !empty($data->transaction_type) &&
    !empty($data->status) &&
    !empty($data->amount) &&
    !empty($data->transaction_code)
){
  
    // set product property values
    $transactions->user_id = $data->user_id;
    $transactions->course_id = $data->course_id;
    $transactions->transaction_type = $data->transaction_type;
    $transactions->status = $data->status;
    $transactions->amount = $data->amount;
    $transactions->transaction_code = $data->transaction_code;
    $transactions->created_by = $data->user_id;
    
    
    
    
    
    $transactions->created_date = date('Y-m-d');
  
    // create the product
    if($transactions->create()){
    
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
       /* print_r($transactions);*/
        echo json_encode(array("message" => "Transaction Created successfully"));
    }
  
    // if unable to create the product, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        
        echo json_encode(array("message" => "Unable to create, Please try again"));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Please Fill mandatory Fields."));
}

?>