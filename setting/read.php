<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here
// include database and object files
include_once '../config/database.php';
include_once '../objects/settings.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$settings = new Settings($db);



  
// read products will be here
// query products
$stmt = $settings->read();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // products array
    $settings_arr=array();
    $settings_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $settings_data=array(
            "name" => $name,
            "value" => $value
        );
         array_push($settings_arr["records"], $settings_data);
         
    }
    
    // set response code - 200 OK
    http_response_code(200);
    
        echo json_encode(
        array($settings_data)
        ); 
    
    
}
  
// no products found will be here
