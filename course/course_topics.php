<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here
// include database and object files
include_once '../config/database.php';
include_once '../objects/course.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$course = new Course($db);



  
// read products will be here
// query products
$stmt = $course->read_content();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // products array
    $course_arr=array();
    $course_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        $course_data=array(
            "course_id" => $course_id,
            "chapterid" => $chapterid,
            "chapter_name" => $chapter_name,
            "sub_chapter_id" => $sub_chapter_id,
            "subchapter_name" => $subchapter_name,
            "type" => $type,
            "sub_type" => $sub_type,
            "pre_class_msg" => $pre_class_msg,
            "post_class_msg" => $post_class_msg,
            "learner_watched" => $learner_watched,
            "show_recording" => $show_recording,
            "available_date" => $available_date,
            "available_time" => $available_time,
            "end_session" => $end_session,
            "recording_link" => $recording_link,
            "path" => $path,
             "created_date" => $created_date,
             "time" => time(),

            
            
        );
         array_push($course_arr["records"], $course_data);
         
    }
    
    // set response code - 200 OK
    http_response_code(200);
    echo json_encode(
        array("courses" => $course_arr["records"])
        );
    
}
  
// no products found will be here
else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no products found
    echo json_encode(
        array("message" => "No Course Found.")
    );
}