<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here
// include database and object files
include_once '../config/database.php';
include_once '../objects/courseprice.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$courseprice = new CoursePrice($db);


// set ID property of record to read
$courseprice->course_id = isset($_GET['course_id']) ? $_GET['course_id'] : die();


  
// read products will be here
// query products
$stmt = $courseprice->read();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // products array
    $courseprice_arr=array();
    $courseprice_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $courseprice_data=array(
            "price_id" => $price_id,
            "course_id" => $course_id,
            "plan_name" => $plan_name,
            "no_of_days" => $no_of_days,
            "original_price" => $original_price,
            "tax" => $tax,
            "payable_price" => $payable_price,
        );
         array_push($courseprice_arr["records"], $courseprice_data);
         
    }
    
    // set response code - 200 OK
    http_response_code(200);
    echo json_encode(
        array("course_plans" => $courseprice_arr["records"])
        ); 
    
}
  
// no products found will be here
else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no products found
    echo json_encode(
        array("message" => "No Plans found.")
    );
}